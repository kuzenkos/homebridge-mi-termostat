module.exports = homebridge => {
  const { TermostatAccessory } = require("./lib/accessory")(homebridge);
  homebridge.registerAccessory(
    "homebridge-mi-termostat",
    "Termostat",
    TermostatAccessory
  );
};
