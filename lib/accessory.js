//todo history contact sensor

const { Scanner } = require("./scanner");
const { version } = require("../package.json");

const fs = require('fs');

let Service;
let Characteristic;
let FakeGatoHistoryService;
let homebridgeAPI;

const defaultTimeout = 15;

class TermostatAccessory {
	constructor(log, config) {
		this.log = log;
		this.config = config || {};
		this.displayName = this.config.name;

		this.latestTemperature = undefined;
		this.latestHumidity = undefined;
		this.latestBatteryLevel = undefined;
		this.lastUpdatedAt = undefined;


		this.targetTemperature = undefined;
		this.contactSensorStatus = undefined;
		this.thermostatStatus = -1;


		this.informationService = this.getInformationService();
		this.temperatureService = this.getTemperatureService();
		this.humidityService = this.getHumidityService();
		this.batteryService = this.getBatteryService();
		this.contactSensorService = this.getContactSensorService();
		this.fakeGatoHistoryService = this.getFakeGatoHistoryService();
		this.fakeGatoContactHistoryService = this.getFakeGatoContactHistoryService();

		this.scanner = this.getScanner();


		this.settings_filename = `./.homebridge/mi_termostat_setting_${this.displayName}.json`;

		this.readStatusFromFile();


		this.log.debug("Initialized accessory");
	}

	set temperature(newValue) {
		this.latestTemperature = newValue;
		this.lastUpdatedAt = Date.now();
		this.temperatureService
			.getCharacteristic(Characteristic.CurrentTemperature)
			.updateValue(newValue)
		;
		this.addFakeGatoHistoryEntry();
	}

	get temperature() {
		// if (this.hasTimedOut()) {
		// 	return undefined;
		// }
		this.calculateContactSensorStatus();
		return this.latestTemperature;
	}

	set humidity(newValue) {
		this.latestHumidity = newValue;
		this.lastUpdatedAt = Date.now();
		this.temperatureService
			.getCharacteristic(Characteristic.CurrentRelativeHumidity)
			.updateValue(newValue)
		;
		this.humidityService
			.getCharacteristic(Characteristic.CurrentRelativeHumidity)
			.updateValue(newValue)
		;
		this.addFakeGatoHistoryEntry();
	}

	get humidity() {
		// if (this.hasTimedOut()) {
		// 	return undefined;
		// }
		return this.latestHumidity;
	}

	set batteryLevel(newValue) {
		this.latestBatteryLevel = newValue;
		this.lastUpdatedAt = Date.now();
		this.batteryService
			.getCharacteristic(Characteristic.BatteryLevel)
			.updateValue(newValue)
		;
	}

	get batteryLevel() {
		// if (this.hasTimedOut()) {
		// 	return undefined;
		// }
		return this.latestBatteryLevel;
	}

	get temperatureName() {
		return this.config.temperatureName || "Temperature";
	}

	get humidityName() {
		return this.config.humidityName || "Humidity";
	}

	get serialNumber() {
		return this.config.address != null
			? this.config.address.replace(/:/g, "")
			: undefined;
	}

	get lastUpdatedISO8601() {
		return new Date(this.lastUpdatedAt).toISOString();
	}

	get fakeGatoStoragePath() {
		return this.config.fakeGatoStoragePath || homebridgeAPI.user.storagePath();
	}

	get timeout() {
		var tm = this.config.timeout == null ? defaultTimeout : this.config.timeout;
		return tm;
	}

	get isFakeGatoEnabled() {
		return this.config.fakeGatoEnabled || false;
	}


	saveStatusToFile() {
		var data = JSON.stringify({
			'targetTemperature': this.targetTemperature,
			'thermostatStatus': this.thermostatStatus
		});

		fs.writeFile(this.settings_filename, data, function(err, data){
			if (err) console.log(err);
			//console.log("Successfully Written to File.");
		});
	}
	
	readStatusFromFile() {
		var that = this;
		fs.readFile(this.settings_filename, 'utf-8' ,function(err, buf) {
			//console.log(buf.toString());
			if(!err){
				var data = JSON.parse(buf.toString());
				that.targetTemperature = data.targetTemperature;
				that.thermostatStatus = data.thermostatStatus;
			}
		});
	}

	getScanner() {
		const scanner = new Scanner(this.config.address, {
			log: this.log,
			forceDiscovering: this.config.forceDiscovering !== false,
			restartDelay: this.config.forceDiscoveringDelay
		});
		scanner.on("temperatureChange", (temperature, peripheral) => {
			const { address } = peripheral;
			this.log.debug(`[${address}] Temperature: ${temperature}C`);
			this.temperature = temperature;
		});
		scanner.on("humidityChange", (humidity, peripheral) => {
			const { address } = peripheral;
			this.log.debug(`[${address}] Humidity: ${humidity}%`);
			this.humidity = humidity;
		});
		scanner.on("batteryChange", (batteryLevel, peripheral) => {
			const { address } = peripheral;
			this.log.debug(`[${address}] Battery level: ${batteryLevel}%`);
			this.batteryLevel = batteryLevel;
		});
		scanner.on("error", error => {
			this.log.error(error);
		});

		return scanner;
	}

	hasTimedOut() {
		if (this.timeout === 0) {
			return false;
		}
		if (this.lastUpdatedAt == null) {
			return false;
		}
		const timeoutMilliseconds = 1000 * 60 * this.timeout;
		const timedOut = this.lastUpdatedAt >= Date.now() - timeoutMilliseconds;
		if (timedOut) {
			this.log.warn(
				`[${this.config.address}] Timed out, last update: ${
				this.lastUpdatedISO8601
				}`
			);
		}
	//	this.log('timeout', timedOut, this.lastUpdatedAt, (Date.now() - timeoutMilliseconds), ( this.lastUpdatedAt - (Date.now() - timeoutMilliseconds)));

		return timedOut;
	}

	addFakeGatoHistoryEntry() {
		if (
			!this.isFakeGatoEnabled ||
			(this.temperature == null || this.humidity == null)
		) {
			return;
		}
		this.fakeGatoHistoryService.addEntry({
			time: new Date().getTime() / 1000,
			temp: this.temperature,
			humidity: this.humidity
		});
	}

	addFakeGatoContactHistoryEntry() {
		if (
			!this.isFakeGatoEnabled
		) {
			return;
		}
		this.fakeGatoContactHistoryService.addEntry({
			time: new Date().getTime() / 1000,
			status: this.contactSensorStatus
		});
	}

	getFakeGatoHistoryService() {
		if (!this.isFakeGatoEnabled) {
			return undefined;
		}
		const serialNumber = this.serialNumber || this.constructor.name;
		const filename = `fakegato-history_termostat_${serialNumber}.json`;
		const path = this.fakeGatoStoragePath;
		return new FakeGatoHistoryService("room", this, {
			filename,
			path,
			storage: "fs"
		});
	}

	getFakeGatoContactHistoryService() {
		if (!this.isFakeGatoEnabled) {
			return undefined;
		}
		const serialNumber = this.serialNumber || this.constructor.name;
		const filename = `fakegato-history_termostat_contact_${serialNumber}.json`;
		const path = this.fakeGatoStoragePath;
		return new FakeGatoHistoryService("door", this, {
			filename,
			path,
			storage: "fs"
		});
	}

	calculateContactSensorStatus() {
	

		if(!(this.thermostatStatus > 0)){
			return;
		}

	//	this.log('buffer contact sensor check', this.targetTemperature - this.latestTemperature);
		if(this.targetTemperature - this.latestTemperature < 0.3 && this.targetTemperature - this.latestTemperature > -0.2){
		//	this.log('buffer contact sensor status', this.latestTemperature, this.targetTemperature, this.contactSensorStatus, this.thermostatStatus, this.targetTemperature - this.latestTemperature);
			return;
		}
 
		var newStatus = 0;
		if (this.latestTemperature < this.targetTemperature){
			newStatus = 1;
		}

		if (newStatus != this.contactSensorStatus) {
			this.contactSensorStatus = newStatus;
			this.log('change contact sensor status', this.latestTemperature, this.targetTemperature, this.contactSensorStatus, this.thermostatStatus);
			this.contactSensorService.setCharacteristic(Characteristic.ContactSensorState, this.contactSensorStatus);
			this.addFakeGatoContactHistoryEntry();
		}
	}

	getInformationService() {
		const accessoryInformation = new Service.AccessoryInformation()
			.setCharacteristic(Characteristic.Manufacturer, "Cleargrass Inc")
			.setCharacteristic(Characteristic.Model, "LYWSDCGQ01ZM")
			.setCharacteristic(Characteristic.FirmwareRevision, version);
		if (this.serialNumber != null) {
			accessoryInformation.setCharacteristic(
				Characteristic.SerialNumber,
				this.serialNumber
			);
		}
		return accessoryInformation;
	}

	onCharacteristicGetValue(field, callback) {
		const value = this[field];
		if (value == null) {
			callback(new Error(`Undefined characteristic value for ${field}`));
		} else {
			callback(null, value);
		}
	}

	getTemperatureService() {
		var that = this;
		const temperatureService = new Service.Thermostat(
			this.temperatureName
		);
		temperatureService
			.getCharacteristic(Characteristic.CurrentTemperature)
			.on("get", this.onCharacteristicGetValue.bind(this, "temperature"))
		;
		temperatureService
			.getCharacteristic(Characteristic.CurrentRelativeHumidity)
			.on("get", this.onCharacteristicGetValue.bind(this, "humidity"))
		;
		temperatureService
			.getCharacteristic(Characteristic.TargetTemperature)
			.on("get", function (callback) {
				callback(null, that.targetTemperature)
			})
			.on("set", function (newValue, callback) {
				that.targetTemperature = newValue;
				that.calculateContactSensorStatus();
				that.saveStatusToFile();
				callback(null);
			})
		;

		temperatureService
			.getCharacteristic(Characteristic.CurrentHeatingCoolingState)
			.on("get", function (callback) {
				callback(null, that.thermostatStatus)
			})
			.setProps({
                format: Characteristic.Formats.UINT8,
                maxValue: 1,
                minValue: 0,
                validValues: [0, 1],
                perms: [Characteristic.Perms.READ, Characteristic.Perms.NOTIFY]
            });
		;
		temperatureService
			.getCharacteristic(Characteristic.TargetHeatingCoolingState)
			.on("get", function (callback) {
				if(-1 == that.thermostatStatus){
					callback('error -1');
				}else{
					callback(null, that.thermostatStatus);
				}
			})
			.on("set", function (newValue, callback) {
				that.thermostatStatus = newValue;
				that.calculateContactSensorStatus();
				that.saveStatusToFile();
				callback(null);
			})
			.setProps({
				format: Characteristic.Formats.UINT8,
				maxValue: 1,
				minValue: 0,
				validValues: [0, 1],
				perms: [Characteristic.Perms.READ, Characteristic.Perms.WRITE, Characteristic.Perms.NOTIFY]
		  	})
		;

		return temperatureService;
	}

	getHumidityService() {
		const humidityService = new Service.HumiditySensor(this.humidityName);
		humidityService
			.getCharacteristic(Characteristic.CurrentRelativeHumidity)
			.on("get", this.onCharacteristicGetValue.bind(this, "humidity"))
		;
		return humidityService;
	}

	getBatteryService() {
		const batteryService = new Service.BatteryService("Battery");
		batteryService
			.getCharacteristic(Characteristic.BatteryLevel)
			.on("get", this.onCharacteristicGetValue.bind(this, "batteryLevel"))
		;
		batteryService.setCharacteristic(Characteristic.ChargingState, 2);
		batteryService
			.getCharacteristic(Characteristic.StatusLowBattery)
			.on("get", this.onCharacteristicGetValue.bind(this, "batteryStatus"))
		;
		return batteryService;
	}

	getContactSensorService() {
		var that = this;
		const contactSensorService = new Service.ContactSensor(this.temperatureName + ' Fake Contact Sensor');
		contactSensorService
			.getCharacteristic(Characteristic.ContactSensorState)
			.on("get", function (callback) {
				callback(null, that.contactSensorStatus)
			})
		;

		return contactSensorService;
	}

	getServices() {
		const services = [
			this.informationService,
			this.temperatureService,
			this.batteryService,
			this.humidityService,
			this.contactSensorService,
			this.fakeGatoHistoryService
		];
		return services.filter(Boolean);
	}
}

module.exports = homebridge => {
	FakeGatoHistoryService = require("fakegato-history")(homebridge);
	Service = homebridge.hap.Service;
	Characteristic = homebridge.hap.Characteristic;
	homebridgeAPI = homebridge;
	return { TermostatAccessory };
};